FROM jupyter/scipy-notebook:latest
LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ARG NB_USER=jovyan

USER root

RUN \
    apt-get update && \
    apt-get -y upgrade
RUN \
    apt-get -y install apt-utils sshpass libpq-dev bzip2 pkg-config \
    libzmq3-dev git graphviz smbclient 


USER ${NB_USER}
RUN mkdir ${HOME}/notebooks 


#### Install extra Python packages #
####################################
RUN \
    conda update conda
RUN \
    conda install -y -c conda-forge pip pandas pymysql mysql psycopg2 \
    SQLAlchemy requests boto3 lxml pytz xlrd pymssql bokeh datashader \
    dask distributed seaborn scikit-learn keras tensorflow graphviz \
    nltk textblob lightfm pysftp dask-ml numba pysmbclient 
RUN \
    python -m textblob.download_corpora
RUN \
    pip install --upgrade pip && \
    pip install git+https://github.com/domoinc/domo-python-sdk.git && \
    pip install --upgrade jupyterthemes && \
    pip install --upgrade git+https://bitbucket.org/yurz/c3py && \
    pip install --upgrade git+https://bitbucket.org/yurz/datautils-python && \
    pip install --upgrade dash && \
    pip install --upgrade dash_core_components && \
    pip install --upgrade dash_html_components

#### Set jupyter theme
##############################
RUN \
    jt -t onedork -fs 12 -altp -tfs 11 -nfs 115 -cellw 96% -T -N





#######################################################################
#######################################################################
#######################################################################
#### TMP layer (to me merged later, avoid long build for now): 
RUN \
    pip install pydbgen && \
    conda install -y -c conda-forge MechanicalSoup selenium 

# Install deps + add Chrome Stable + purge all the things
USER root
RUN apt-get update && apt-get install -y \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg \
	--no-install-recommends \
	&& curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
	&& apt-get update && apt-get install -y \
	google-chrome-stable \
	fontconfig \
	fonts-ipafont-gothic \
	fonts-wqy-zenhei \
	fonts-thai-tlwg \
	fonts-kacst \
	fonts-symbola \
	fonts-noto \
	--no-install-recommends 
	# && rm -rf /var/lib/apt/lists/*

# Install Firefox
# RUN apt-get install -y --no-install-recommends firefox

RUN conda install -y -c conda-forge openpyxl xlsxwriter 

USER root
RUN apt-get update && apt-get -y install tzdata
ENV TZ Australia/Melbourne

USER ${NB_USER}

#######################################################################
#######################################################################
#######################################################################





#### Cleanup
##############################
USER root
RUN \
    conda clean -y -a && \
    apt-get clean autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/{cache,log}/
RUN chown -R ${NB_USER} /home/${NB_USER}


USER ${NB_USER}
WORKDIR ${HOME}/notebooks
