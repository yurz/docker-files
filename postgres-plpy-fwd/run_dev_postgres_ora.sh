#!/usr/bin/env bash
docker stop dev-postgres
docker rm dev-postgres
docker run --name dev-postgres \
--net='host' --dns-search='local' \
-d -p 5432:5432 \
-e POSTGRES_USER=pg \
-e POSTGRES_PASSWORD=pgpass \
-v /media/app_data/pg01:/var/lib/postgresql \
-v /media/app_data/pg01/data:/var/lib/postgresql/data \
--memory='8g' \
yurz/postgres-plpy-fwd-ora 
# -e TERM=xterm \
# -e LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib \
# -e ORACLE_HOME=/usr/lib/oracle/12.1/client64 \