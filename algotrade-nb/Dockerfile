FROM jupyter/scipy-notebook:latest
LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ARG NB_USER=jovyan

USER root

COPY ta-lib/ . 

RUN \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install apt-utils sshpass libpq-dev bzip2 pkg-config \
    libzmq3-dev git graphviz smbclient tzdata \
    libfreetype6-dev libpng-dev libopenblas-dev liblapack-dev gfortran && \
    ./configure --prefix=/usr && make && make install

ENV TZ Australia/Melbourne

USER ${NB_USER}
RUN \ 
    mkdir ${HOME}/notebooks && \
    conda update conda && \
    conda install -y -c conda-forge numpy pip pandas psycopg2 \
    SQLAlchemy requests boto3 lxml pytz xlrd \
    dask distributed matplotlib seaborn graphviz \
    # bokeh scikit-learn keras tensorflow nltk textblob dask-ml lightfm \ 
    pysftp numba && \
    pip install --upgrade pip \
    git+https://bitbucket.org/yurz/datautils-python \
    git+https://bitbucket.org/yurz/c3py \
    cufflinks plotly dash dash_core_components \
    dash_html_components python-telegram-bot \
    jupyterthemes TA-Lib && \
    jupyter labextension install @mflevine/jupyterlab_html jupyterlab-drawio @jupyterlab/plotly-extension && \
    jt -t onedork -fs 12 -altp -tfs 11 -nfs 115 -cellw 96% -T -N

#### Cleanup
##############################
USER root
RUN \
    conda clean -y -a && \
    apt-get clean autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/{cache,log}/
RUN chown -R ${NB_USER} /home/${NB_USER}


USER ${NB_USER}
WORKDIR ${HOME}/notebooks

# docker build -t yurz/algotrade-nb:latest .
