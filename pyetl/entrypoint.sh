#!/bin/sh

# get app code from S3
aws s3 cp $S3_PATH /app --recursive --exclude "*.log"

cd /app 

# install any adhoc python libraries if required:
if [ -z "$PY_REQ_FILE" ]
then
    echo "no PY_REQ_FILE provided"
else
    pip install --no-cache-dir --requirement $PY_REQ_FILE
fi

# app entry command to execute on docker run 
# can be python or bash script or any executable
# for example:
#     python my_script.py
#     bash my_script.sh
# 'python app.py' by executed by default 
eval "${APP_ENTRY:-python app.py}"
