FROM yurz/pybase:latest

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get -y --no-install-recommends update \
    && conda install --yes --freeze-installed -c conda-forge nomkl \
        pip pandas \
        pandas requests lxml pytz SQLAlchemy psycopg2 \
        xlrd openpyxl xlsxwriter geopandas joblib \
        pysftp plotly \
    && pip install --no-cache-dir awscli boto3 awswrangler \
        git+https://bitbucket.org/yurz/datautils-python \
        httplib2 google-api-python-client oauth2client \
    && conda clean --all --force-pkgs-dirs --yes \
    && find /miniconda/ -follow -type f -name '*.a' -delete \
    && find /miniconda/ -follow -type f -name '*.pyc' -delete \
    && find /miniconda/ -follow -type f -name '*.js.map' -delete \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint.sh /app/entrypoint.sh
COPY run_jupyter.sh /app/run_jupyter.sh
RUN \
    chmod +x /app/entrypoint.sh \
    && chmod +x /app/run_jupyter.sh 

WORKDIR /app
CMD ["/app/entrypoint.sh"]


# docker build --squash -t yurz/pyetl:aws-ecs-latest -f pyetl-aws-ecs.dockerfile .
