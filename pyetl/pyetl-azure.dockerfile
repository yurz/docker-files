FROM yurz/pybase:latest

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get -y update \
    && apt-get -y install git gnupg2 \
    && conda update conda \
    && conda install python=3.6.7 \
    && conda install -y -c conda-forge pip pandas azure \
        requests lxml pytz SQLAlchemy psycopg2 pymssql mysql \
        xlrd openpyxl xlsxwriter \
        pysftp pysmbclient \
    && pip install --upgrade \
        git+https://bitbucket.org/yurz/datautils-python \
    && mkdir /app 

#### AZURE SQL Driver ####
RUN \
    curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get update \
    && ACCEPT_EULA=Y apt-get -y install msodbcsql17  unixodbc unixodbc-dev \
    && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile \
    && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc \
    && exec bash \
    && . ~/.bashrc \
    && pip install pyodbc 
RUN \ 
    conda install -c anaconda pyodbc
##########################


### tmp layer ###
RUN \
    pip install git+https://github.com/matthieudelaro/akeneo_api_client#egg=akeneo_api_client \
    && pip install --upgrade google-api-python-client \
    && pip install oauth2client 

################



#### Cleanup #################
##############################
RUN \
    conda clean -y -a && \
    apt-get clean autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/{cache,log}/


WORKDIR /app
CMD ["python", "./app.py"]