FROM yurz/pyetl:aws-ecs-latest

LABEL authors="Yuri Zhylyuk yuri@zhylyuk.com"

ENV DEBIAN_FRONTEND noninteractive

COPY ta-lib/ /app/ta-lib/ 

RUN \
    apt-get update \
    && apt-get -y --no-install-recommends install apt-utils libpq-dev bzip2 pkg-config \
        liblapack-dev gfortran \
    && cd /app/ta-lib \
    && /app/ta-lib/configure --prefix=/usr && make && make install \
    && pip install --no-cache-dir TA-Lib \
    && cd / \
    && rm -rf /app/ta-lib \
    && conda clean --all --force-pkgs-dirs --yes \
    && find /miniconda/ -follow -type f -name '*.a' -delete \
    && find /miniconda/ -follow -type f -name '*.pyc' -delete \
    && find /miniconda/ -follow -type f -name '*.js.map' -delete \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/{cache,log}/

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh 

WORKDIR /app
CMD ["/entrypoint.sh"]


# docker build --squash -t yurz/pyetl:aws-ecs-ta-latest -f pyetl-aws-ecs-ta.dockerfile .
