docker run -it --rm \
-p 8888:8888 \
--net='host' \
--dns-search='local' \
-v "$(pwd)"/app:/app \
-v ~/.aws:/root/.aws \
-e PY_REQ_FILE=/app/py_req_sample.txt \
-e APP_ENTRY="bash run_jupyter_sample.sh" \
yurz/pyetl:aws-ecs-ta-latest


# -e APP_ENTRY="python my_script_sample.py" \